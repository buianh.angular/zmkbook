import { Router } from "express";
import role from "./../../controllers/role/role.controller";
import { authorizationMiddeleware } from "./../../middleware/authorizationMiddleware";

// import { PublsForm } from "./../../controllers/publicsher/validate-form/form-publsvalid";
// import { validFormMiddleware } from "./../../middleware/validFormMiddleware";
const roleRouter = Router();
roleRouter.post("/role/create-role", authorizationMiddeleware("admin"), role.createItemRole);
roleRouter.get("/role", authorizationMiddeleware("user"), role.getAllDataRole);
roleRouter.put("/role/update-role/:id", authorizationMiddeleware("admin"), role.updateItemRole);
roleRouter.delete("/role/delete-role/:id", authorizationMiddeleware("admin"), role.deleteItemRole);
roleRouter.get("/role/:id", authorizationMiddeleware("admin"), role.findDataRoleById);
export default roleRouter;
