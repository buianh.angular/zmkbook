import { Router } from "express";
import bookinstance from "./../../controllers/bookinstance/bookinstance.controller";
import { BookValidForm } from "./../../controllers/bookinstance/validate-form/form-bookisvalid";
import { validFormMiddleware } from "./../../middleware/validFormMiddleware";
import { authorizationMiddeleware } from "./../../middleware/authorizationMiddleware";
const bookInsRouter = Router();
bookInsRouter.get("/bookinstance", authorizationMiddeleware("all"), bookinstance.getAllDataBookInstance);
bookInsRouter.get("/bookinstance/:id", authorizationMiddeleware("all"), bookinstance.getDataBookInstanceById);
bookInsRouter.post(
    "/bookinstance/create-bookins",
    authorizationMiddeleware("admin"),
    validFormMiddleware(BookValidForm, "body"),
    bookinstance.createDataBookInstance
);
bookInsRouter.put(
    "/bookinstance/update-bookins/:id",
    authorizationMiddeleware("admin"),
    validFormMiddleware(BookValidForm, "body"),
    bookinstance.updateDataBookInstance
);
bookInsRouter.delete("/bookinstance/delete-bookins/:id", authorizationMiddeleware("admin"), bookinstance.deleteDataBookInstance);

export default bookInsRouter;
