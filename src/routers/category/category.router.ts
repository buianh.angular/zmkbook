import { Router } from "express";
import category from "./../../controllers/category/category.controller";
import { validFormMiddleware } from "./../../middleware/validFormMiddleware";
import { authorizationMiddeleware } from "./../../middleware/authorizationMiddleware";
import { CateForm } from "./../../controllers/category/validate-form/form-catevali";
const cateRouter = Router();
cateRouter.get("/cate", authorizationMiddeleware("all"), category.getAllCategory);
cateRouter.get("/cate/:id", authorizationMiddeleware("all"), category.findCateById);
cateRouter.post("/cate/create-cate", authorizationMiddeleware("admin"), validFormMiddleware(CateForm, "body"), category.createItemCategory);
cateRouter.put(
    "/cate/update-cate/:id",
    authorizationMiddeleware("admin"),
    validFormMiddleware(CateForm, "body"),
    category.updateItemCategory
);
cateRouter.delete("/cate/delete-cate/:id", authorizationMiddeleware("admin"), category.deleteItemCategory);
export default cateRouter;
