import { Router } from "express";
import account from "./../../controllers/account/account.controller";
import { authorizationMiddeleware } from "./../../middleware/authorizationMiddleware";
const accountRouter = Router();
accountRouter.post("/account/register", account.registerAccount);
accountRouter.post("/account/login", account.login);
accountRouter.post("/account/edit-personal-infomation/:id", authorizationMiddeleware("user"), account.editPersonalInfomation);
accountRouter.put("/account/changle-password/:id", authorizationMiddeleware("user"), account.changlePassword);
export default accountRouter;
