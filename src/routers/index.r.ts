import cateRouter from "./category/category.router";
import publsRouter from "./publicsher/publicsher.router";
import authorRouter from "./author/author.router";
import bookRouter from "./book/book.router";
import bookInsRouter from "./bookinstance/bookinstance.router";
import accountRouter from "./account/account.router";
import roleRouter from "./role/role.router";
import borrowbookRouter from "./borrowbook/borrowbook.router";
export default {
    cateRouter,
    publsRouter,
    authorRouter,
    bookRouter,
    bookInsRouter,
    roleRouter,
    accountRouter,
    borrowbookRouter,
};
