import { Router } from "express";
import publisher from "./../../controllers/publicsher/publicsher.controller";
import { PublsForm } from "./../../controllers/publicsher/validate-form/form-publsvalid";
import { validFormMiddleware } from "./../../middleware/validFormMiddleware";
import { authorizationMiddeleware } from "./../../middleware/authorizationMiddleware";
const publsRouter = Router();
publsRouter.post(
    "/publisher/create-publisher",
    authorizationMiddeleware("admin"),
    validFormMiddleware(PublsForm, "body"),
    publisher.createItemPublichser
);
publsRouter.get("/publisher", authorizationMiddeleware("all"), publisher.getAllDataPublicsher);
publsRouter.put(
    "/publisher/update-publisher/:id",
    authorizationMiddeleware("admin"),
    validFormMiddleware(PublsForm, "body"),
    publisher.updateItemPublicsher
);
publsRouter.delete("/publisher/delete-publisher/:id", authorizationMiddeleware("admin"), publisher.deletePublicsher);
publsRouter.get("/publisher/:id", authorizationMiddeleware("all"), publisher.findPublicsherById);
export default publsRouter;
