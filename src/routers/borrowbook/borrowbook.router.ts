import { Router } from "express";
import { authorizationMiddeleware } from "./../../middleware/authorizationMiddleware";
import borrowbook from "./../../controllers/borrowbook/borrowbook.controller";

const borrowbookRouter = Router();
borrowbookRouter.post("/borrowbook", authorizationMiddeleware("user"), borrowbook.borrowBooks);
borrowbookRouter.get("/borrowbook/get-borrowbook-by-id-user", authorizationMiddeleware("user"), borrowbook.getBorrowBookByIdUser);
borrowbookRouter.put("/update-review-about-book/:id", authorizationMiddeleware("user"), borrowbook.updateReviewAboutBook);
borrowbookRouter.put("/lets-user-borrow-book/:id", authorizationMiddeleware("admin"), borrowbook.letsUserBorrowBook);
borrowbookRouter.put("/confirm-return-book/:id", authorizationMiddeleware("admin"), borrowbook.comfirmReturnBooks);
export default borrowbookRouter;
