import { Router } from "express";
import author from "./../../controllers/author/author.controller";
import { AuthorForm } from "./../../controllers/author/validate-form/form-catevalid";
import { validFormMiddleware } from "./../../middleware/validFormMiddleware";
import { authorizationMiddeleware } from "./../../middleware/authorizationMiddleware";
const authorRouter = Router();
authorRouter.get("/author", authorizationMiddeleware("all"), author.getAllDataAuthor);
authorRouter.get("/author/:id", authorizationMiddeleware("all"), author.findAuthorById);
authorRouter.post(
    "/author/create-author",
    authorizationMiddeleware("admin"),
    validFormMiddleware(AuthorForm, "body"),
    author.createItemAuthor
);
authorRouter.put(
    "/author/update-author/:id",
    authorizationMiddeleware("admin"),
    validFormMiddleware(AuthorForm, "body"),
    author.updateItemAuthor
);
authorRouter.delete("/author/delete-author/:id", authorizationMiddeleware("admin"), author.deleteItemAuthor);

export default authorRouter;
