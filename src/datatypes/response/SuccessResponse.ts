export const SuccessResponse = (statusCode?: any, message?: string, data?: any) => ({
    code: statusCode,
    success: true,
    message: message,
    data: data,
});
