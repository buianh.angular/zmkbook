export const ErrorResponse = (statusCode?: number, message?: string, data?: any) => ({
    code: statusCode,
    error: true,
    message: message,
    data: data,
});
