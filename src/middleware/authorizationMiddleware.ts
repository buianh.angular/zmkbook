import { Request, Response, NextFunction } from "express";
import { ErrorResponse } from "./../datatypes/response/ErrorResponse";
import jsonwebtoken from "jsonwebtoken";
import dotenv from "dotenv";
import { getManager } from "typeorm";
import { Role } from "./../entity/Role";
import { Account } from "./../entity/Account";
dotenv.config();
export const authorizationMiddeleware = (type) => (req: Request, res: Response, next: NextFunction) => {
    let accessToken: any = req.headers["x-access-token"];
    if (!accessToken) return res.status(400).json(ErrorResponse(400, "Token is not exist"));
    try {
        jsonwebtoken.verify(accessToken, process.env.TOKEN_SECRET, async function (err, data) {
            if (err) res.status(400).json(ErrorResponse(400, "Error authorization", err));
            let dataUser = await getManager()
                .createQueryBuilder(Role, "role")
                .where("role.account = :account", { account: data.foo })
                .execute();

            let checkType: string[] = ["all", "user"];
            for (const item of dataUser) {
                if (checkType.includes(type) == true) {
                    res.locals.user = item.role_accountId;
                    return next();
                } else if (item.role_name == type) {
                    res.locals.user = item.role_accountId;
                    return next();
                }
                if (item.role_name == "user") {
                    res.locals.user = item.role_accountId;
                    return next();
                }
            }
            return res.status(400).json(ErrorResponse(400, "role mismatch"));
        });
    } catch (error) {
        return res.status(400).json(ErrorResponse(400, "Error authorization"));
    }
};
