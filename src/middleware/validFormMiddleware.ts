import { NextFunction, Request, Response } from "express";
import { validate, ValidationError } from "class-validator";
import { plainToClass } from "class-transformer";
import { ErrorResponse } from "./../datatypes/response/ErrorResponse";
// export type ValidateErrors = {
//     [field: string]: {
//         [constraintName: string]: string;
//     };
// };
// export const ValidateErrorResponse = (
//     validateErrors: ValidateErrors
// ): {
//     error: "VALIDATE_ERROR";
//     message: string;
//     data: ValidateErrors;
// } => ({
//     error: "VALIDATE_ERROR",
//     message: "validate error",
//     data: validateErrors,
// });
// const convertErrors = (errors: ValidationError[]): ValidateErrors => {
//     let result: any = {};
//     for (let err of errors) {
//         if (err.children && err.children.length > 0) {
//             result[err.property] = convertErrors(err.children);
//         } else {
//             result[err.property] = Object.keys(err.constraints).map((key) => ({
//                 value: err.value,
//                 code: key,
//                 message: err.constraints[key],
//             }));
//         }
//     }
//     return result;
// };

// export const validForm = async <T>(
//     formClass: { new (...args): T },
//     data,
//     groups?: string[]
// ): Promise<{
//     form?: T;
//     errors?: ValidateErrors;
// }> => {
//     let form = plainToClass(formClass, [data], { groups })[0];
//     let errors = await validate(form, {
//         groups,
//     });
//     if (errors && errors.length > 0) {
//         let validateErrors: ValidateErrors = convertErrors(errors);
//         return { errors: validateErrors };
//     }
//     return { form };
// };
export const validFormMiddleware = <T>(formClass: { new (): T; [type: string]: any }, reqProperty: "body" | "query" | "params") => async (
    req: Request,
    res: Response,
    next: NextFunction
) => {
    let form = plainToClass(formClass, req[reqProperty]);
    let result: any = {};
    validate(form).then((errors) => {
        for (let item of errors) {
            // obj.data khi chỉ trỏ tới 1 biến cụ thể not object
            // nếu biến đó là 1 object mà bên trong có nhiều trường ta lên sử dụng obj[data.item]
            console.log(item);
            result[item.property] = Object.keys(item.constraints).map((key) => ({
                value: item.value == undefined || "" ? "" : item.value,
                code: key,
                message: item.constraints[key],
            }));
        }
        if (errors) res.status(400).json(ErrorResponse(400, "validation error", result));
    });
};
