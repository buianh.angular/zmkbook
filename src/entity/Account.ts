import { Entity, Column, CreateDateColumn, UpdateDateColumn, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { Role } from "./Role";
import { BorrowBook } from "./BorrowBook";

@Entity()
export class Account {
    constructor(data?: Partial<Account>) {
        Object.assign(this, data);
    }

    @PrimaryGeneratedColumn("uuid")
    id: string;

    @Column()
    name: string;

    @Column()
    phone: number;

    @Column()
    email: string;

    @Column()
    cmnn_cc: string;

    @Column({ default: false })
    status: boolean;

    @Column()
    password: string;

    @Column()
    day_of_birth: Date;

    @Column()
    activation_code: string;

    @CreateDateColumn()
    createdAt: Date;

    @UpdateDateColumn()
    updatedAt: Date;

    @OneToMany((type) => Role, (role) => role.account, { cascade: true })
    roles: Role[];

    @OneToMany((type) => BorrowBook, (borrowbook) => borrowbook.account, { cascade: true })
    borrowbooks: BorrowBook[];
}
