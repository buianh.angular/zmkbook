import { Entity, Column, CreateDateColumn, UpdateDateColumn, PrimaryGeneratedColumn, OneToMany } from "typeorm";
import { Book } from "./Book";
@Entity()
export class Publisher {
    constructor(data?: Partial<Publisher>) {
        Object.assign(this, data);
    }
    @PrimaryGeneratedColumn("uuid")
    id: string;

    @Column()
    name: string;

    @Column()
    description: string;

    @CreateDateColumn()
    createdAt: Date;

    @UpdateDateColumn()
    updatedAt: Date;

    @OneToMany((type) => Book, (book) => book.publisher)
    books: Book[];
}
