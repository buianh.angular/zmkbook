import { PrimaryGeneratedColumn, Entity, Column, CreateDateColumn, ManyToOne } from "typeorm";
import { type } from "os";
import { Account } from "./Account";
import { Role } from "./Role";
import { BookInstance } from "./BookInstance";
@Entity()
export class BorrowBook {
    constructor(data?: Partial<BorrowBook>) {
        Object.assign(this, data);
    }
    @PrimaryGeneratedColumn("uuid")
    id: string;

    @Column({ default: "new book 90%" })
    status_before: string;

    @Column({ default: null })
    status_after?: string;

    @Column()
    expected_borrow_date: Date;

    @Column({ default: null })
    actual_borrow_date?: Date;

    @Column()
    expected_return_date?: Date;

    @Column({ default: null })
    actual_return_date?: Date;

    @Column({ default: null })
    review?: string;

    @ManyToOne((type) => Account, (account) => account.borrowbooks, { onDelete: "CASCADE" })
    account: Account;

    @ManyToOne((type) => BookInstance, (bookinstance) => bookinstance.borrowbooks, { onDelete: "CASCADE" })
    bookinstance: BookInstance;
}
