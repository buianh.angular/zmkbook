import { ManyToOne, OneToMany, Entity, Column, PrimaryGeneratedColumn } from "typeorm";
import { Author } from "./Author";
import { Category } from "./Category";
import { Publisher } from "./Publicsher";
import { BookInstance } from "./BookInstance";
@Entity()
export class Book {
    @PrimaryGeneratedColumn("uuid")
    id: string;

    @Column()
    name: string;

    @Column()
    description: string;

    @Column()
    price: number;

    @ManyToOne((type) => Author, (author) => author.books, { onDelete: "CASCADE" })
    author: Author;

    @ManyToOne((type) => Category, (category) => category.books, { onDelete: "CASCADE" })
    category: Book;

    @ManyToOne((type) => Publisher, (publisher) => publisher.books, { onDelete: "CASCADE" })
    publisher: Publisher;

    @OneToMany((type) => BookInstance, (bookinstance) => bookinstance.book, { cascade: true })
    bookinstances: Book[];
}
