import { Entity, Column, PrimaryGeneratedColumn, ManyToOne, OneToMany } from "typeorm";
import { Account } from "./Account";
import { BorrowBook } from "./BorrowBook";
@Entity()
export class Role {
    constructor(data?: Partial<Role>) {
        Object.assign(this, data);
    }
    @PrimaryGeneratedColumn("uuid")
    id: string;

    @Column()
    name: string;

    @ManyToOne((type) => Account, (account) => account.roles, { onDelete: "CASCADE" })
    account: Account;
}
