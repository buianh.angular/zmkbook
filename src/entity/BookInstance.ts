import { Entity, ManyToOne, Column, PrimaryGeneratedColumn, OneToMany } from "typeorm";
import { Book } from "./Book";
import { BorrowBook } from "./BorrowBook";
@Entity()
export class BookInstance {
    constructor(data?: Partial<BookInstance>) {
        Object.assign(this, data);
    }
    @PrimaryGeneratedColumn("uuid")
    id: string;

    @ManyToOne((type) => Book, (book) => book.bookinstances, { onDelete: "CASCADE" })
    book: Book;

    @Column()
    code: string;

    @Column({ default: false })
    status: boolean;

    @OneToMany((type) => BorrowBook, (borrowbook) => borrowbook.bookinstance, { cascade: true })
    borrowbooks: BorrowBook[];
}
