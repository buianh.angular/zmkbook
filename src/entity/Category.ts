import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, UpdateDateColumn, OneToMany } from "typeorm";
import { Book } from "./Book";
@Entity()
export class Category {
    constructor(data?: Partial<Category>) {
        Object.assign(this, data);
    }
    @PrimaryGeneratedColumn("uuid")
    id: string;

    @Column()
    name: string;

    @Column()
    description: string;

    @CreateDateColumn()
    createdAt: Date;

    @UpdateDateColumn()
    updatedAt: Date;

    @OneToMany((type) => Book, (book) => book.category, { cascade: true })
    books: Book[];
}
