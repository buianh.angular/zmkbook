import nodemailer from "nodemailer";
import dotenv from "dotenv";
dotenv.config();
const host = process.env.MAILHOST;
const port = +process.env.MAILPORT;
const account = process.env.MAILACCOUNT;
const password = process.env.MAILPASSWORD;
export const sendMailer = (to, subject, htmlContent?: string) => {
    let transporter = nodemailer.createTransport({
        port: port,
        host: host,
        secure: false,
        auth: {
            user: account,
            pass: password,
        },
        tls: {
            rejectUnauthorized: false,
        },
    });
    let into = {
        from: account,
        to: to,
        subject: subject,
        html: `<h2>
                <a href="https://www.google.com/search?q=google+d%E1%BB%8Bch&oq=go%C3%B4&aqs=chrome.3.69i57j0l7.4549j0j7&sourceid=chrome&ie=UTF-8">${htmlContent}</a>
            </h2>`,
    };
    return transporter.sendMail(into);
};
