import { Account } from "./../../entity/Account";
import { getManager } from "typeorm";
import { Request, Response } from "express";
import { ErrorResponse } from "./../../datatypes/response/ErrorResponse";
import { SuccessResponse } from "./../../datatypes/response/SuccessResponse";
import bcrypt from "bcrypt";
import { sendMailer } from "./../../utils/sendMail";
import { ramdomString } from "./../../utils/activationCode";
import { Role } from "./../../entity/Role";
import jsonwebtoken from "jsonwebtoken";
import dotenv from "dotenv";
dotenv.config();
const saltRounds = 10;
let salt = bcrypt.genSaltSync(saltRounds);
let registerAccount = async (req: Request, res: Response) => {
    try {
        let { name, phone, email, password, day_of_birth, cmnn_cc } = req.body;
        password = bcrypt.hashSync(password, salt);
        let activation_code = ramdomString(6);
        const item = new Account({ name, phone, password, email, day_of_birth, activation_code, cmnn_cc });
        let checkEmailExist = await getManager().findOne(Account, { email: item.email });
        if (checkEmailExist) {
            res.status(400).json(ErrorResponse(400, "email already exists, please choose another email"));
            return;
        }
        let createData = await getManager().save(Account, item);
        await getManager().save(Role, { name: "user", account: createData });
        sendMailer(item.email, "Đăng ký tài khoản thành công", "Chuyển tới zmk book đê bắt đầu mượn sách");
        res.status(200).send(
            SuccessResponse(200, "create account successfully, please go to in your gmail to activate your account", createData)
        );
    } catch (error) {
        res.status(400).json(ErrorResponse(400, "create account error", error));
    }
};
let login = async (req: Request, res: Response) => {
    try {
        let { email, password } = req.body;
        let item = { email, password };
        let user = await getManager().findOne(Account, { email: item.email });
        if (!user) return res.status(400).json(ErrorResponse(400, "username or password incorrectly"));
        // if (dataLogin.status) return res.status(400).json(ErrorResponse(404, "The account has not been activated "));
        let comparePassword = await bcrypt.compare(password, user.password);
        if (!comparePassword) return res.status(400).json(ErrorResponse(400, "username or password incorrectly"));
        jsonwebtoken.sign({ foo: user.id }, process.env.TOKEN_SECRET, { expiresIn: "24h" }, function (err, data) {
            if (err) res.status(400).json(ErrorResponse(400, "Jwt sign error:", err));
            return res.status(200).json(SuccessResponse(200, "login successfully", data));
        });
    } catch (error) {
        res.status(400).json(ErrorResponse(400, "Login failed", error));
    }
};
let editPersonalInfomation = async (req: Request, res: Response) => {
    try {
        let { id } = req.params;
        let { name, phone, email, day_of_birth, cmnn_cc } = req.body;
        let item = { name, phone, email, day_of_birth, cmnn_cc };
        let dataEdit = await getManager().update(Account, id, item);
        res.status(200).json(SuccessResponse(200, "edit personal information successfuly", dataEdit));
    } catch (error) {
        res.status(200).json(ErrorResponse(400, "edit personal information error", error));
    }
};
let changlePassword = async (req: Request, res: Response) => {
    try {
        let { id } = req.params;
        let { password } = req.body;
        let dataAccount = await getManager().findOne(Account, id);
        if (!dataAccount) return res.status(400).json(SuccessResponse(200, "account not exist"));
        password = bcrypt.hashSync(password, salt);
        let editPassword = await getManager().update(Account, id, password);
        res.status(200).json(SuccessResponse(200, "edit personal information successfuly", editPassword));
    } catch (error) {
        res.status(200).json(ErrorResponse(400, "edit personal information error", error));
    }
};
export default {
    registerAccount,
    login,
    editPersonalInfomation,
    changlePassword,
};
