import { Request, Response } from "express";
import { Category } from "./../../entity/Category";
import { getConnection, getManager } from "typeorm";
import { SuccessResponse } from "./../../datatypes/response/SuccessResponse";
import { ErrorResponse } from "./../../datatypes/response/ErrorResponse";
import { CateForm } from "./validate-form/form-catevali";
import { validate } from "class-validator";
let getAllCategory = async (req: Request, res: Response) => {
    try {
        let getAllCategory = await getManager().createQueryBuilder(Category, "category").getMany();
        res.status(200).json(SuccessResponse(200, "get all data category", getAllCategory));
    } catch (error) {
        res.status(400).json(ErrorResponse(404, "get data category error", error));
    }
};

let findCateById = async (req: Request, res: Response) => {
    try {
        let { id } = req.params;
        let findCategoryById = await getManager().createQueryBuilder(Category, "category").where("category.id = :id", { id: id }).getOne();
        res.status(200).json(SuccessResponse(200, "data id category ", findCategoryById));
    } catch (error) {
        res.status(400).json(ErrorResponse(404, "get data id category error", error));
    }
};
let createItemCategory = async (req: Request, res: Response) => {
    try {
        let { name, description } = req.body;

        let item = { name, description };
        let dataCreate = await getConnection().createQueryBuilder().insert().into(Category).values(item).execute();
        res.status(200).json(SuccessResponse(200, "create category successfully", dataCreate));
    } catch (error) {
        console.log(error);
        res.status(500).json(ErrorResponse(500, "create category error", error));
    }
};
let updateItemCategory = async (req: Request, res: Response) => {
    try {
        let { id } = req.params;
        let { name, description } = req.body;
        let item = { name, description };
        await getConnection().createQueryBuilder().update(Category).set(item).where("id = :id", { id: id }).execute();
        res.status(200).json(SuccessResponse(200, "upadte category sucessfuly", ""));
    } catch (error) {
        res.status(500).json(ErrorResponse(500, "update category error", error));
    }
};
let deleteItemCategory = async (req: Request, res: Response) => {
    try {
        let { id } = req.params;
        let dataDelete = await getConnection().createQueryBuilder().delete().from(Category).where("id = :id", { id: id }).execute();
        res.status(200).json(SuccessResponse(200, "delete category successfuly", ""));
    } catch (error) {
        res.status(404).json(ErrorResponse(404, "delete category error", error));
    }
};
export default {
    getAllCategory,
    createItemCategory,
    findCateById,
    updateItemCategory,
    deleteItemCategory,
};
