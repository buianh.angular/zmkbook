import { IsNotEmpty, IsString } from "class-validator";
export class CateForm {
    @IsNotEmpty()
    @IsString()
    name: string;

    @IsNotEmpty()
    @IsString()
    description: string;
}
