import { IsNotEmpty, IsString } from "class-validator";
export class BookValidForm {
    @IsNotEmpty()
    @IsString()
    book: string;
}
