import { Request, Response } from "express";
import { getManager, getRepository } from "typeorm";
import { BookInstance } from "./../../entity/BookInstance";
import { SuccessResponse } from "./../../datatypes/response/SuccessResponse";
import { ErrorResponse } from "./../../datatypes/response/ErrorResponse";
import { ramdomString } from "./../../utils/activationCode";

let createDataBookInstance = async (req: Request, res: Response) => {
    try {
        let { book } = req.body;
        let item = { code: ramdomString(5), book };
        let dataCreate = await getManager().save(BookInstance, item);
        res.status(200).json(SuccessResponse(200, "create book instance successfullu", dataCreate));
    } catch (error) {
        res.status(400).json(ErrorResponse(400, "create book error", error));
    }
};
let updateDataBookInstance = async (req: Request, res: Response) => {
    try {
        let { id } = req.params;
        let { code, book } = req.body;
        let item = { code: ramdomString(5), book };
        await getManager().update(BookInstance, id, item);
        res.status(200).json(SuccessResponse(200, "update book instance successfully"));
    } catch (error) {
        res.status(400).json(ErrorResponse(400, "update book error", error));
    }
};
let deleteDataBookInstance = async (req: Request, res: Response) => {
    try {
        let { id } = req.params;
        await getManager().delete(BookInstance, id);
        res.status(200).json(SuccessResponse(200, "delete book instance successfully"));
    } catch (error) {
        res.status(400).json(ErrorResponse(400, "delete book error", error));
    }
};
let getAllDataBookInstance = async (req: Request, res: Response) => {
    try {
        let bookRepository = getRepository(BookInstance);
        let dataBook = await bookRepository.find({ relations: ["book"] });
        res.status(200).json(SuccessResponse(200, "data book", dataBook));
    } catch (error) {
        res.status(400).json(ErrorResponse(400, "data book error", error));
    }
};
let getDataBookInstanceById = async (req: Request, res: Response) => {
    try {
        let { id } = req.body;
        let bookRepository = getRepository(BookInstance);
        let dataBook = await bookRepository.findOne(id, { relations: ["book"] });
        res.status(200).json(SuccessResponse(200, "data book", dataBook));
    } catch (error) {
        res.status(400).json(ErrorResponse(400, "data book error", error));
    }
};
export default {
    getDataBookInstanceById,
    createDataBookInstance,
    updateDataBookInstance,
    deleteDataBookInstance,
    getAllDataBookInstance,
};
