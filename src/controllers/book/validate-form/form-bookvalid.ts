import { IsNotEmpty, IsString, IsNumber } from "class-validator";
export class BookForm {
    @IsNotEmpty()
    @IsString()
    name: string;

    @IsNotEmpty()
    @IsString()
    description: string;

    @IsNotEmpty()
    @IsNumber()
    price: number;

    @IsNotEmpty()
    @IsString()
    author: string;

    @IsNotEmpty()
    @IsString()
    publisher: string;

    @IsNotEmpty()
    @IsString()
    category: string;
}
