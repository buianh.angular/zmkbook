import { Request, Response } from "express";
import { Book } from "../../entity/Book";
import { getManager, getRepository } from "typeorm";
import { SuccessResponse } from "./../../datatypes/response/SuccessResponse";
import { ErrorResponse } from "./../../datatypes/response/ErrorResponse";
let getAllDataBook = async (req: Request, res: Response) => {
    try {
        let bookRepository = getRepository(Book);
        let dataBook = await bookRepository.find({ relations: ["category", "publisher", "author"] });
        return res.status(200).json(SuccessResponse(200, "Data book", dataBook));
    } catch (error) {
        return res.status(400).send(ErrorResponse(400, "data book error", error));
    }
};
let findDataBookById = async (req: Request, res: Response) => {
    try {
        let { id } = req.params;
        let bookRepository = getRepository(Book);
        let dataBookById = await bookRepository.findOne(id, { relations: ["category", "publisher", "author"] });
        if (!dataBookById) return res.status(400).send(ErrorResponse(400, "data book not exist"));
        res.status(200).json(SuccessResponse(200, "Data book by id", dataBookById));
    } catch (error) {
        res.status(400).send(ErrorResponse(400, "data book by id error", error));
    }
};
let createItemBook = async (req: Request, res: Response) => {
    try {
        let { name, description, price, author, category, publisher } = req.body;
        let item = { name, description, price, author, category, publisher };
        let dataCreate = await getManager().save(Book, item);
        res.status(200).send(SuccessResponse(200, "create book successfully", dataCreate));
    } catch (error) {
        res.status(400).send(ErrorResponse(400, "create book error", error));
    }
};
let updateItemBook = async (req: Request, res: Response) => {
    try {
        let { id } = req.params;
        let { name, description, price, author, category, publisher } = req.body;
        let item = { name, description, price, author, category, publisher };
        let dataUpdateBook = await getManager().update(Book, id, item);
        if (dataUpdateBook.affected == 0) return res.status(400).send(ErrorResponse(400, "data book not exist"));
        res.status(200).json(SuccessResponse(200, "update book successfully", dataUpdateBook));
    } catch (error) {
        res.status(400).json(ErrorResponse(400, "update book error", error));
    }
};
let deleteItemBook = async (req: Request, res: Response) => {
    try {
        let { id } = req.params;
        let dataDeleteBook = await getManager().delete(Book, id);
        if (dataDeleteBook.affected == 0) return res.status(400).send(ErrorResponse(400, "data book not exist"));

        res.status(200).json(SuccessResponse(200, "delete book successfully"));
    } catch (error) {
        res.status(400).json(ErrorResponse(400, "delete book error", error));
    }
};
let searchBook = async (req: Request, res: Response) => {
    try {
        let { keySearch } = req.query;
        let dataSearch = await getManager()
            .createQueryBuilder(Book, "book")
            .where("name like :name", { name: "%" + keySearch + "%" })
            .getMany();
        res.status(200).json(SuccessResponse(200, "data search", dataSearch));
    } catch (error) {
        res.status(400).json(ErrorResponse(400, "search error", error));
    }
};
export default {
    createItemBook,
    getAllDataBook,
    findDataBookById,
    updateItemBook,
    deleteItemBook,
    searchBook,
};
