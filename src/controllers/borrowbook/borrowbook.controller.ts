import { Request, Response } from "express";
import { getManager } from "typeorm";
import { BorrowBook } from "./../../entity/BorrowBook";
import { SuccessResponse } from "./../../datatypes/response/SuccessResponse";
import { ErrorResponse } from "./../../datatypes/response/ErrorResponse";
import { BookInstance } from "../../entity/BookInstance";
let borrowBooks = async (req: Request, res: Response) => {
    try {
        if (!res.locals.user) return res.status(400).send(ErrorResponse(400, "id user not exist"));
        let { bookinstance, expected_borrow_date, expected_return_date } = req.body;
        if (Number(new Date(expected_borrow_date)) < Number(Date.now()))
            return res.status(400).send(ErrorResponse(400, "expected day can't be less than then current day "));
        if (Number(new Date(expected_borrow_date)) > Number(new Date(expected_return_date)))
            return res.status(400).send(ErrorResponse(400, "expected borreturnrow day can't be less than then expected borrow day"));
        let item = { account: res.locals.user, bookinstance, expected_borrow_date, expected_return_date };
        let checkStatusBookInstance = await getManager().findOne(BookInstance, bookinstance);
        if (checkStatusBookInstance.status) return res.status(400).send(ErrorResponse(400, "book has been borrowed"));
        let borrowBook = await getManager().save(BorrowBook, item);
        return res.status(200).send(SuccessResponse(200, "borrow books successfully", borrowBook));
    } catch (error) {
        return res.status(400).send(ErrorResponse(400, "borrow books error", error));
    }
};
let getBorrowBookByIdUser = async (req: Request, res: Response) => {
    try {
        let getBooks = await getManager()
            .createQueryBuilder(BorrowBook, "borrowbook")
            .leftJoinAndSelect("borrowbook.account", "account")
            .where("borrowbook.account = :account", { account: res.locals.user })
            .getMany();
        if (getBooks.length < 0) return res.status(400).json(ErrorResponse(400, "not books were found by user"));
        return res.status(200).json(SuccessResponse(200, "borrow book by id user", getBooks));
    } catch (error) {
        return res.status(400).send(ErrorResponse(400, "error find borrow book by id", error));
    }
};
let updateReviewAboutBook = async (req: Request, res: Response) => {
    try {
        let { id } = req.params;
        let item = { review: req.body.review };
        await getManager().update(BorrowBook, id, item);
        return res.status(200).json(SuccessResponse(200, "update review about book successfully"));
    } catch (error) {
        return res.status(400).send(ErrorResponse(400, "update review about book error", error));
    }
};
let letsUserBorrowBook = async (req: Request, res: Response) => {
    try {
        let { id } = req.params;
        let dataBrrowBooks = await getManager()
            .createQueryBuilder(BorrowBook, "borrowbook")
            .leftJoinAndSelect("borrowbook.bookinstance", "bookinstance")
            .where("borrowbook.id = :id", { id: id })
            .getOne();
        if (dataBrrowBooks.bookinstance.status) return res.status(400).json(ErrorResponse(400, "book instance has been others borrowed"));
        await getManager().update(BorrowBook, id, { actual_borrow_date: new Date(Date.now()) });
        await getManager().update(BookInstance, dataBrrowBooks.bookinstance.id, { status: true });
        res.status(200).json(SuccessResponse(200, "lets user borrow book successfully"));
    } catch (error) {
        res.status(400).json(ErrorResponse(400, "lets user borrow book error", error));
    }
};
let comfirmReturnBooks = async (req: Request, res: Response) => {
    try {
        let { id } = req.params;
        let dataBrrowBooks = await getManager()
            .createQueryBuilder(BorrowBook, "borrowbook")
            .leftJoinAndSelect("borrowbook.bookinstance", "bookinstance")
            .where("borrowbook.id = :id", { id: id })
            .getOne();
        if (!dataBrrowBooks.bookinstance.status) return res.status(400).json(ErrorResponse(400, "book instance not yet person borrowed"));
        await getManager().update(BorrowBook, id, { actual_return_date: new Date(Date.now()), status_after: req.body.status_after });
        await getManager().update(BookInstance, dataBrrowBooks.bookinstance.id, { status: false });
        res.status(200).json(SuccessResponse(200, "lets user borrow book successfully"));
    } catch (error) {
        res.status(400).json(ErrorResponse(400, "lets user borrow book error", error));
    }
};
export default {
    borrowBooks,
    getBorrowBookByIdUser,
    updateReviewAboutBook,
    letsUserBorrowBook,
    comfirmReturnBooks,
};
