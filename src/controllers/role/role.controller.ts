import { Role } from "./../../entity/Role";
import { Request, Response } from "express";
import { getManager, createQueryBuilder, getRepository } from "typeorm";
import { SuccessResponse } from "./../../datatypes/response/SuccessResponse";
import { ErrorResponse } from "./../../datatypes/response/ErrorResponse";
import { Account } from "../../entity/Account";
let getAllDataRole = async (req: Request, res: Response) => {
    try {
        let postRepository = getRepository(Role);
        let dataRole = await postRepository.find({ relations: ["account"] });
        console.log(res.locals.user);

        res.status(200).json(SuccessResponse(200, "get data role successfully", dataRole));
    } catch (error) {
        res.status(400).json(SuccessResponse(400, "data role error", error));
    }
};
let findDataRoleById = async (req: Request, res: Response) => {
    try {
        let { id } = req.params;
        let postRepository = getRepository(Role);
        let dataRole = await postRepository.findOne(id, { relations: ["account"] });
        res.status(200).json(SuccessResponse(200, "get data role successfully", dataRole));
    } catch (error) {
        res.status(400).json(SuccessResponse(400, "data role error", error));
    }
};
let createItemRole = async (req: Request, res: Response) => {
    try {
        let { name, account } = req.body;
        let item = new Role({ name, account });
        let checkIdAccout = await getManager()
            .createQueryBuilder(Role, "role")
            .where("role.account = :account", { account: account })
            .getMany();
        if (checkIdAccout.length >= 2) return res.status(400).send(ErrorResponse(400, "an account has only two role"));
        let checkAccoutOfPermissions = await getManager()
            .createQueryBuilder(Role, "role")
            .where("role.account= :account", { account: account })
            .andWhere("name =:name", { name: name })
            .getMany();
        for (const item of checkAccoutOfPermissions) {
            if (item.name == name) {
                return res.status(400).send(ErrorResponse(400, "role has exist, please choose new role"));
            }
        }
        let createData = await getManager().save(Role, item);
        res.status(200).send(SuccessResponse(200, "create role for account successfully", createData));
    } catch (error) {
        console.log(error);

        res.status(400).send(ErrorResponse(400, "create role for account error", error));
    }
};
let updateItemRole = async (req: Request, res: Response) => {
    try {
        let { id } = req.params;
        let { name, account } = req.body;
        let item = new Role({ name, account });
        let checkAccoutOfPermissions = await getManager()
            .createQueryBuilder(Role, "role")
            .where("role.account= :account", { account: account })
            .andWhere("name =:name", { name: name })
            .getMany();
        for (const item of checkAccoutOfPermissions) {
            if (item.name == name) {
                return res.status(400).send(ErrorResponse(400, "role has exist, please choose new role"));
            }
        }
        await getManager().update(Role, id, item);
        res.status(200).send(SuccessResponse(200, "update role for account successfully"));
    } catch (error) {
        res.status(400).send(ErrorResponse(400, "update role for account error", error));
    }
};
let deleteItemRole = async (req: Request, res: Response) => {
    try {
        let { id } = req.params;
        let createData = await getManager().delete(Role, id);
        res.status(200).send(SuccessResponse(200, "update role for account successfully", createData));
    } catch (error) {
        res.status(400).send(ErrorResponse(400, "update role for account error", error));
    }
};
export default {
    getAllDataRole,
    createItemRole,
    findDataRoleById,
    updateItemRole,
    deleteItemRole,
};
