import { Request, Response } from "express";
import { Author } from "./../../entity/Author";
import { getConnection, getManager } from "typeorm";
import { SuccessResponse } from "./../../datatypes/response/SuccessResponse";
import { ErrorResponse } from "./../../datatypes/response/ErrorResponse";
let getAllDataAuthor = async (req: Request, res: Response) => {
    try {
        let dataAuthor = await getManager().createQueryBuilder(Author, "author").getMany();
        res.status(200).json(SuccessResponse(200, "create author ", dataAuthor));
    } catch (error) {
        res.status(400).json(ErrorResponse(400, "data author error", error));
    }
};
let findAuthorById = async (req: Request, res: Response) => {
    try {
        let { id } = req.params;
        let findAuthorById = await getManager().createQueryBuilder(Author, "author").where("id = :id", { id: id }).getOne();
        res.status(200).json(SuccessResponse(200, "create author ", findAuthorById));
    } catch (error) {
        res.status(400).json(ErrorResponse(400, "data author error", error));
    }
};
let createItemAuthor = async (req: Request, res: Response) => {
    try {
        let { name, description } = req.body;
        let item = { name, description };
        let dataCreate = await getConnection().createQueryBuilder().insert().into(Author).values(item).execute();
        res.status(200).json(SuccessResponse(200, "create author successfully", dataCreate));
    } catch (error) {
        res.status(400).json(ErrorResponse(400, "create author error", error));
    }
};
let updateItemAuthor = async (req: Request, res: Response) => {
    try {
        let { id } = req.params;
        let { name, description } = req.body;
        await getConnection().createQueryBuilder().update(Author).set({ name, description }).where("id = :id", { id: id }).execute();
        res.status(200).json(SuccessResponse(200, "update author successfully"));
    } catch (error) {
        res.status(400).json(ErrorResponse(400, "update author error", error));
    }
};
let deleteItemAuthor = async (req: Request, res: Response) => {
    try {
        let { id } = req.params;
        await getConnection().createQueryBuilder().delete().from(Author).where("id = :id", { id: id }).execute();
        res.status(200).json(SuccessResponse(200, "delete author successfully"));
    } catch (error) {
        res.status(400).json(ErrorResponse(400, "update author error", error));
    }
};
export default {
    createItemAuthor,
    updateItemAuthor,
    deleteItemAuthor,
    getAllDataAuthor,
    findAuthorById,
};
