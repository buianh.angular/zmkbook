import { Publisher } from "./../../entity/Publicsher";
import { getConnection, getManager } from "typeorm";
import { Request, Response } from "express";
import { SuccessResponse } from "./../../datatypes/response/SuccessResponse";
import { ErrorResponse } from "./../../datatypes/response/ErrorResponse";

let createItemPublichser = async (req: Request, res: Response) => {
    try {
        let { name, description } = req.body;
        let item = { name, description };
        let dataCreate = await getConnection().createQueryBuilder().insert().into(Publisher).values(item).execute();
        res.status(200).json(SuccessResponse(200, "create publicsher successfully", dataCreate));
    } catch (error) {
        res.status(400).json(ErrorResponse(400, "create publicsher error", error));
    }
};
let getAllDataPublicsher = async (req: Request, res: Response) => {
    try {
        let dataPublicsher = await getManager().createQueryBuilder(Publisher, "publicsher").getMany();
        res.status(200).json(SuccessResponse(200, "data publicsher ", dataPublicsher));
    } catch (error) {
        res.status(400).json(ErrorResponse(400, "gett data publicsher error", error));
    }
};
let findPublicsherById = async (req: Request, res: Response) => {
    try {
        let { id } = req.params;
        let dataIdPublicsher = await getManager().createQueryBuilder(Publisher, "publicsher").where("id = :id", { id: id }).getOne();
        res.status(200).json(SuccessResponse(200, "data publicsher by id", dataIdPublicsher));
    } catch (error) {
        res.status(400).json(ErrorResponse(400, "gett data publicsher error", error));
    }
};
let updateItemPublicsher = async (req: Request, res: Response) => {
    try {
        let { id } = req.params;
        let { name, description } = req.body;
        let item = { name, description };
        await getConnection().createQueryBuilder().update(Publisher).update(item).where("id = :id", { id: id }).execute();
        res.status(200).json(SuccessResponse(200, "update publicsher successfully"));
    } catch (error) {
        res.status(400).json(ErrorResponse(400, "update publicsherr error", error));
    }
};
let deletePublicsher = async (req: Request, res: Response) => {
    try {
        let { id } = req.params;
        await getConnection().createQueryBuilder().delete().from(Publisher).where("id = :id", { id: id }).execute();
        res.status(200).json(SuccessResponse(200, "delete publicsher successfully"));
    } catch (error) {
        res.status(400).json(ErrorResponse(400, "update publicsherr error", error));
    }
};
export default {
    createItemPublichser,
    getAllDataPublicsher,
    updateItemPublicsher,
    deletePublicsher,
    findPublicsherById,
};
