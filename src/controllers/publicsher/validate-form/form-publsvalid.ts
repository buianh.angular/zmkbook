import { IsNotEmpty, IsString } from "class-validator";
export class PublsForm {
    @IsNotEmpty()
    @IsString()
    name: string;

    @IsNotEmpty()
    @IsString()
    description: string;
}
