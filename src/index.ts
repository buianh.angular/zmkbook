import express, { Application, Request, Response, NextFunction } from "express";
import bodyParser from "body-parser";
import cookieParser from "cookie-parser";
import http from "http";
import { createConnection } from "typeorm";
import dotenv from "dotenv";
import routers from "./routers/index.r";
import { Category } from "./entity/Category";
import { Publisher } from "./entity/Publicsher";
import { Author } from "./entity/Author";
import { Book } from "./entity/Book";
import { Account } from "./entity/Account";
import { Role } from "./entity/Role";
import { BookInstance } from "./entity/BookInstance";
import { BorrowBook } from "./entity/BorrowBook";
const app: Application = express();
let server = new http.Server(app);
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cookieParser());
app.use("/API", routers.cateRouter);
app.use("/API", routers.publsRouter);
app.use("/API", routers.authorRouter);
app.use("/API", routers.bookRouter);
app.use("/API", routers.bookInsRouter);
app.use("/API", routers.accountRouter);
app.use("/API", routers.roleRouter);
app.use("/API", routers.borrowbookRouter);

app.get("*", (req: Request, res: Response) => {
    res.status(404).json({ error: "Not found" });
});
app.post("*", (req: Request, res: Response) => {
    res.status(404).json({ error: "Not found" });
});
app.put("*", (req: Request, res: Response) => {
    res.status(404).json({ error: "Not found" });
});
app.delete("*", (req: Request, res: Response) => {
    res.status(404).json({ error: "Not found" });
});
dotenv.config();

let { HOST, PORT } = process.env;
let startServer = async () => {
    console.log("Creating postgres database connection...");
    try {
        await createConnection({
            type: "postgres",
            host: "localhost",
            port: 5432,
            username: "postgres",
            password: "123456",
            database: "zmkbook",
            synchronize: true,
            logging: false,
            entities: [Category, Publisher, Author, Book, BookInstance, Account, Role, BorrowBook],
        });
        console.log("Connect to database successfully");
    } catch (error) {
        console.log("Connect to database error");
        throw error;
    }
    server.listen(PORT, () => {
        console.log(`Connecting to ${HOST}: ${PORT} successfully`);
    });
};

startServer();
